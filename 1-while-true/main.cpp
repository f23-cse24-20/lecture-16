#include <iostream>
#include <ucm_random>
using namespace std;

int main() {
    
    /*
        Determine the number of attempts it takes for the random number generator 
        to generate a specific number in a given range.

        Sample Input: 7 1 100
        
        Sample Output: It took 39 attempts to randomly generate a 7 in the range [1, 100]
    */


    RNG generator;
    int count = 0;

    int target, min, max;
    cin >> target >> min >> max;

    while (true) {
        int x = generator.get(min, max);
        count++;

        if (x == target) {
            break;
        }
    }

    cout << "It took "  << count << " attempts to randomly generate a " << target << " in the range [" << min << ", " << max << "]" << endl;


    return 0;
}