#include <iostream>
using namespace std;

int main() {

    /*
        Count the number of vowels in a string.

        Sample Input: COMPUTER
        
        Sample Output: COMPUTER has 3 vowels
    */


    string str = "computer";
    int count = 0;

    for (int i = 0; i < str.length(); i++) {
        char letter = toupper(str[i]);
        if (letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'U') {
            count++;
        }
    }

    cout << str << " has " << count << " vowels" << endl;

    return 0;
}