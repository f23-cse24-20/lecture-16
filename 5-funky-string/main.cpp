#include <iostream>
using namespace std;

int main() {

    /*
        Convert a string into a funky string.

        Sample Input: COMPUTER
        
        Sample Output: cOmPuTeR
    */


    // char letter = 'G';

    // cout << letter << endl;

    // letter = tolower(letter);

    // cout << letter << endl;

    string str = "COMPUTER";

    cout << "Original: " << str << endl;
    for (int i = 0; i < str.length(); i++) {
        if (i % 2 == 0) {
            str[i] = tolower(str[i]);
        } else {
            str[i] = toupper(str[i]);
        }
    }    

    cout << "Modified: " << str << endl;

    return 0;
}