#include <iostream>
using namespace std;

int main() {

    /*
        Print a dash '-' between every character of the string.

        Sample Input: COMPUTER
        
        Sample Output: C-O-M-P-U-T-E-R
    */

    string str = "COMPUTER";

    for (int i = 0; i < str.length(); i++) {
        cout << str[i];

        if (i < str.length() - 1) {
            cout << "-";
        }
    }
    cout << endl;

    return 0;
}